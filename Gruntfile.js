

module.exports = function(grunt) {
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({


        watch:{
            sass:{
                files:['app/assets/styles/*.sass'],
                tasks:['compass:dev','autoprefixer']
            },
            css:{
                files:['app/assets/styles/*.css'],
                tasks:''                          //'autoprefixer'
            }

        },
        express:{
            all:{
                options:{
                    port:3000,
                    hostname:'localhost',
                    bases:['./app'],
                    livereload:true
                }
            }
        },
        compass: {
            dev: {
                options: {
                    sassDir: 'app/assets/styles',
                    cssDir: 'app/assets/styles'

                }
            }
        },
        autoprefixer:{
            dist:{
                files:{
                    'app/assets/styles/app.css':'app/assets/styles/app.css'
                }
            }
        }


    });
    grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {

        grunt.task.run([
            'express',
            'watch'
        ]);
    });

};