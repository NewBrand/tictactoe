/**
 * Created by kamil on 04.02.17.
 */
angular.module('TicTacToe')
    .directive('heroBox', function () {
        return {
            scope:{
                hero: '='
            },
            templateUrl: '../../../assets/views/heroBox.html',
            restrict: 'E',
            controller: function($scope,players){
                $scope.getLabel = function (difficulty) {
                    return players.getDifficultyLabel(difficulty);
                }

            }
        };
    });
