/**
 * Created by kamil on 04.02.17.
 */
angular.module('TicTacToe')
    .controller('chooseHeroCtrl', function ($scope, heros,players,$location) {
        $scope.heros=heros.getHeros();
        $scope.active = 0;

        $scope.confirmHero=function(){
            players.setHero($scope.heros[$scope.active]);
            $location.url('heros-presentation');
        }
    });