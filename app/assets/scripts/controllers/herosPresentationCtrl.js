/**
 * Created by kamil on 05.02.17.
 */
angular.module('TicTacToe')
    .controller('herosPresentationCtrl', function ($scope, players, $location, $timeout) {


        if (!($scope.player = players.getPlayer())) {
            $location.url('')
        }


        $scope.opponent = players.getOpponent();
        $scope.delay = 11;

        var updateCounter = function () {


            if ($scope.delay > 0) {
                var timeout = $timeout(updateCounter, 1000);
            } else {
                $timeout.cancel(timeout);
                $location.url('game');
            }
            $scope.delay--;
        };
        updateCounter();
    });