/**
 * Created by kamil on 04.02.17.
 */
angular.module('TicTacToe')
    .controller('gameCtrl', function ($scope, players, game, $location, $timeout) {

        var _playerStarts = Math.random()>=0.5;

        var baseTime = 10;

        var timeout;


        var setWinner = function (array) {
            array.forEach(function (element) {
                $scope.winnerCells[element[0]][element[1]] = true;
            })
        };

        var checkIsDraw = function () {
            var isDraw = true;
            $scope.board.forEach(function (line) {
                line.forEach(function (cell) {
                    
                    if (!cell) {

                        isDraw = false;
                        return 0;
                    }

                })
            });
            return isDraw;
        };

        var updateCounter = function () {


            if ($scope.counter > 0) {
                timeout = $timeout(updateCounter, 1000);
            } else {
                $scope.winnerInfo = $scope.opponent;
                $scope.winner = 'o';
                $timeout.cancel(timeout);

            }
            $scope.counter--;
        };

        $scope.player = players.getPlayer();
        $scope.opponent = players.getOpponent();

        if (!$scope.player) {
            $location.url('');
        }
        if ($scope.player) {

            var time = Math.round(baseTime / $scope.player.difficulty || 1);
        }

        $scope.counter = time;
        $scope.winner = null;
        $scope.winnerInfo = null;
        $scope.winnerCells = [
            [false, false, false],
            [false, false, false],
            [false, false, false]
        ];

        $scope.getLabel = function (difficulty) {
            return players.getDifficultyLabel(difficulty);
        };


        $scope.board = null;


        game.initGame(_playerStarts).then(function (response) {
            updateCounter();
            var data = response.data;
            game.setToken(data.token);
            $scope.board = data.game.board;
        });


        $scope.makeMove = function (x, y) {
            $timeout.cancel(timeout);
            if (!$scope.winner) {


                game.makeMove(x, y).then(function (response) {
                    var data = response.data;
                    $scope.counter = time;
                    $scope.board = data.game.board;
                   
                    if (data.game.winner) {

                        $scope.winner = data.game.winner.figure;
                        if ($scope.winner == game.getPlayerFigure) {
                            $scope.winnerInfo = $scope.player;

                        } else {
                            $scope.winnerInfo = $scope.opponent;

                        }
                        setWinner(data.game.winner.match);

                    } else if (checkIsDraw()) {
                        $scope.winner = 'd';

                    } else {
                        updateCounter();
                    }
                });
            }

        }
    });