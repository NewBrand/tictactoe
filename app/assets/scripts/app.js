'use strict';

// Declare app level module which depends on views, and components
angular.module('TicTacToe', [
    'ngRoute',
    'ui.bootstrap',
    'ngAnimate'
]).config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $routeProvider.when('/', {
        templateUrl: '../../assets/views/start.html',
        controller: 'startCtrl'
    })
        .when('/choose-your-hero', {
            templateUrl: '../../assets/views/chooseHero.html',
            controller: 'chooseHeroCtrl'
        })
        .when('/heros-presentation', {
            templateUrl: '../../assets/views/herosPresentation.html',
            controller: 'herosPresentationCtrl'
        })
        .when('/game', {
            templateUrl: '../../assets/views/game.html',
            controller: 'gameCtrl'
        })

        .otherwise({redirectTo: '/'});
}]);

