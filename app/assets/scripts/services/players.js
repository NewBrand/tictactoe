angular.module('TicTacToe')
    .factory('players', function (heros) {
        var _player = null;
        var _opponent = null;

        function selectOpponent() {
            var opponents = heros.getHeros();
            var opponentsCount = opponents.length;

            var randomIndex = Math.floor(Math.random() * opponentsCount);
            _opponent = opponents[randomIndex];
            if (_opponent.name == _player.name) {
                selectOpponent()
            }
        }

        function difficultyLabel(difficulty) {

            switch (difficulty) {
                case 1:
                    return 'Easy';
                case 2:
                    return 'Normal';
                case 3:
                    return 'Hard';
                case 4:
                    return 'Hardcore';
            }
        }


        return {
            setHero: function (player) {
                _player = player;
                selectOpponent();
            },
            getPlayer: function () {
                return _player;
            },
            getOpponent: function () {
                return _opponent;
            },
            getDifficultyLabel: difficultyLabel

        }


    });