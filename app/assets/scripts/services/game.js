angular.module('TicTacToe')
    .factory('game', function ($http) {


        var _apiUrl = "http://interview.314.tt/api/game";
        var _gameToken = null;
        var _playerFigure = 'x';


        function _initGame(playerStarts) {
                

            var request = $http({
                method: 'POST',
                url: _apiUrl,
                data: {
                    player: _playerFigure,
                    playerStarts: playerStarts,
                }
            });
            return request;

        }

        function _makeMove(x, y) {
            var request = $http({
                method: 'PUT',
                url: _apiUrl,
                data: {
                    x: x,
                    y: y,
                },
                headers: {
                    'token': _gameToken
                }
            });
            return request;
        }

        return {
            initGame: _initGame,
            makeMove: _makeMove,
            setToken: function (token) {
                _gameToken = token;
            },
            getPlayerFigure: _playerFigure
        }


    });