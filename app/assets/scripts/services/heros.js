/**
 * Created by kamil on 04.02.17.
 */

angular.module('TicTacToe')
    .factory('heros', function () {
        var _herosInfo = [
            {
                name: 'Python',
                avatar: 'assets/images/Python.png',
                philosophy: 'Beautiful is better than ugly',
                roleName: 'General-purpose',
                difficulty: 1,
                pros: [
                    "Clear syntax",
                    "Easy to start",
                    "Lots of tutorials",
                    "Dynamically typed"
                ],
                cons: [
                    "No compile-time checking of types",
                    "Language fragmentation",
                    "Assignment",
                    "dynamically typed"

                ]

            },
            {
                name: 'C',
                avatar: 'assets/images/C.png',
                philosophy: 'Build things as simply as possible',
                roleName: 'General-purpose',
                difficulty: 3,
                pros: [
                    "Understanding of computers",
                    "Industry standard",
                    "Helps with learning other languages later",
                    "Teaches good practices"
                ],
                cons: [
                    "Long learning curve",
                    "Requires memory management",
                    "Completely lacks type safety",
                    "Heavily outdated programming concepts"

                ]

            },
            {
                name: 'JavaScript',
                avatar: 'assets/images/JavaScript.png',
                philosophy: 'Freedom',
                roleName: 'Web Development',
                difficulty: 1,
                pros: [
                    "First-class functions with lexical closures",
                    "Required for web development",
                    "No installation required",
                    "Extremely popular"
                ],
                cons: [
                    "Has really bad parts you're better off avoiding altogether",
                    "Many errors pass silently",
                    "Inconsistent type conversion",
                    "Dependency on browser developer(s)"

                ]

            },
            {
                name: 'Dart',
                avatar: 'assets/images/Dart.png',
                philosophy: 'Easy to learn, easy to scale, and deployable everywhere',
                roleName: 'General-purpose',
                difficulty: 1,
                pros: [
                    "Simple OO language with powerful SDK, VM and built in libraries",
                    "Optional type system",
                    "One way of getting stuff done",
                    "Great fit for the client-side apps"
                ],
                cons: [
                    "Not widely used",
                    "Bad reflection when compiled to JavaScript"

                ]

            },
            {
                name: 'Ruby',
                avatar: 'assets/images/Ruby.png',
                philosophy: "A programmer's best friend",
                roleName: 'General-purpose',
                difficulty: 2,
                pros: [
                    "A Large Ecosystem of tools & libraries",
                    "Clean syntax",
                    "Widely used",
                    "Ruby on Rails"
                ],
                cons: [
                    "Its ecosystem is limited outside of web development",
                    "Meta-programming causes confusion for new developers",
                    "Dynamically typed",
                    "More than one way to do it"

                ]

            },
            {
                name: 'C#',
                avatar: 'assets/images/C_sharp.jpg',
                philosophy: 'Objects from Microsoft',
                roleName: 'General-purpose',
                difficulty: 2,
                pros: [
                    "Supports some functional features",
                    "Incredibly Well-Engineered Language",
                    "Awesome IDE for Windows (but IDE's from JetBrains still better)",
                    "Great introduction to object-oriented programmin"
                ],
                cons: [
                    "Complex syntax",
                    "Lacks standard-library support for immutable data structures",
                    "Dependecy on IDE"

                ]

            },
            {
                name: 'Scheme',
                avatar: 'assets/images/Scheme.jpg',
                philosophy: 'Minimalist design',
                roleName: 'General-purpose',
                difficulty: 2,
                pros: [
                    "Great, well known textbooks",
                    "Simple syntax",
                    "Robust metaprogramming",
                    "No Magic - it's clear how everything works"
                ],
                cons: [
                    "Fragmented ecosystem"

                ]

            },
            {
                name: 'Java',
                avatar: 'assets/images/Java.png',
                philosophy: 'Write once, run anywhere',
                roleName: 'General-purpose',
                difficulty: 3,
                pros: [
                    "Fantastic IDEs",
                    "Most commonly used language in industry",
                    "Consistent programming standards",
                    "Introduces you to object oriented languages"
                ],
                cons: [
                    "Worst-of-both-worlds static type system",
                    "Confusing mis-features",
                    "Locks you into the static OOP mindset",
                    "Garbage collection may teach bad habits"

                ]

            },
            {
                name: 'Elixir',
                avatar: 'assets/images/Elixir.png',
                philosophy: 'For building scalable and maintainable applications',
                roleName: 'General-purpose',
                difficulty: 2,
                pros: [
                    "Clean syntax",
                    "Great community",
                    "Built-in virtual machine"
                ],
                cons: [
                    "Not that popular"

                ]

            },
            {
                name: 'Haskell',
                avatar: 'assets/images/Haskell.png',
                philosophy: 'Declarative, statically typed code.',
                roleName: 'General-purpose',
                difficulty: 4,
                pros: [
                    "Mathematical consistency",
                    "Highly transferable concepts",
                    "Quick Feedback",
                    "Very few language constructs"
                ],
                cons: [
                    "Difficult learning curve",
                    "Way too complex for a first language",
                    "Language extensions lead to unfamiliar code",
                    "Symbols everywhere"

                ]

            },
            {
                name: 'Clojure',
                avatar: 'assets/images/Clojure.png',
                philosophy: 'Robust, practical, and fast',
                roleName: 'General-purpose',
                difficulty: 3,
                pros: [
                    "There is one syntax rule to learn",
                    "Immutability as the default",
                    "A holistic and well designed language",
                    "Simplicity as a pillar of culture"
                ],
                cons: [
                    "Depends on Java for basic tasks",
                    "Confusing error messages",
                    "Syntax is pretty different from most programming languages",
                    "Functional programming languages make a lot of code abstract"

                ]

            },
            {
                name: 'C++',
                avatar: 'assets/images/C_plus_plus.jpg',
                philosophy: 'Programmers should be free to pick their own programming style, and that style should be fully supported by C++',
                roleName: 'General-purpose',
                difficulty: 4,
                pros: [
                    "Huge language supports most everything",
                    "Powerful memory management",
                    "Teaches fundamental OOP",
                    "Teaches problem solving"
                ],
                cons: [
                    "Undefined behavior",
                    "Retains nearly all bad habits of C",
                    "Huge language gets in the way of learning",
                    "No two programmers can agree on which 10% subset of C++ to use"

                ]

            },
            {
                name: 'Scala',
                avatar: 'assets/images/Scala.png',
                philosophy: 'Object-Oriented Meets Functional',
                roleName: 'General-purpose',
                difficulty: 4,
                pros: [
                    "Immutable values",
                    "Multiparadigm",
                    "Compiles to JVM bytecode",
                    "Very good online courses"
                ],
                cons: [
                    "Static type system inherits cruft from Java",
                    "Can be intimidating for beginners",
                    "Way too complex for beginners"

                ]

            },
            {
                name: 'PHP',
                avatar: 'assets/images/php.png',
                philosophy: 'lorem',
                roleName: 'Web Development but also used as general_purpose lang.',
                difficulty: 1,
                pros: [
                    "One of the most common languages",
                    "Lots of tutorials online",
                    "Used by most common CMS platforms",
                    "Most prominent language for web applications"
                ],
                cons: [
                    "Poorly designed language"

                ]

            },
            {
                name: 'Assembly',
                avatar: 'assets/images/Assembly.png',
                philosophy: 'lorem',
                roleName: 'Embedded',
                difficulty: 4,
                pros: [
                    "Low Level - it's how the computer works",
                    "Useful for embedded systems",
                    "Uniform syntax",
                    "Naturally creates fast and small programs"
                ],
                cons: [
                    "Difficult learning curve",
                    "Rarely a requirement or used in professional employment",
                    "Hyperspecific syntax isn't a good first step to learning other modern languages",
                    "Not very portable"

                ]

            },

        ];
        return {
            getHeros: function () {
                return _herosInfo;
            }
        }


    });